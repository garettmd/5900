#!/usr/bin/env python

import argparse
import pexpect
from subprocess import Popen, PIPE
import sys
import time


parser = argparse.ArgumentParser(description='Set up IRF on a pair of 5900s')
parser.add_argument('-i', '--ip', required=True, help='ser2net ip address')
parser.add_argument('-d', '--domain', required=True, help='IRF domain')
parser.add_argument('-p1', '--port1', required=True, help='ser2net port for 1st 5900')
parser.add_argument('-p2', '--port2', required=True, help='ser2net port for 2nd 5900')
parser.add_argument('-y1', '--priority1', required=True, help='member priority for 1st 5900')
parser.add_argument('-y2', '--priority2', required=True, help='member priority for 2nd 5900')
parser.add_argument('-m1', '--member1', required=True, help='1st 5900 member number')
parser.add_argument('-m2', '--member2', required=True, help='2nd 5900 member number')
parser.add_argument('-f1', '--interface1', required=True, help='1st 5900 - primary irf interface')
parser.add_argument('-o1', '--outerface1', required=True, help='1st 5900 - secondary irf interface')
parser.add_argument('-f2', '--interface2', required=True, help='2nd 5900 - primary irf interface')
parser.add_argument('-o2', '--outerface2', required=True, help='2nd 5900 - secondary irf interface')
parser.add_argument('-s', '--description', required=True, help='description of irf domain')
parser.add_argument('-a', '--agg', required=True, metavar='NUMBER', help='Bridge-Aggregation number, i.e. Bridge-Aggregation13 for stack 1')
parser.add_argument('--skip-member-config', nargs='+', choices=['1', '2'],
                    help='Skip member config for a member. Useful if an earlier member config timed out waiting for the switch to come back up. Can specify more than one member, separated by space.')
parser.add_argument('--skip-5900', nargs='+', choices=['1', '2'])
parser.add_argument('--fans', help='Fix fan direction', action='store_true')  # TODO: Make sure you don't need a boolean or something for this
parser.add_argument('--skip-agg', help='Skip the BridgeAggregation step', action='store_true')
args = parser.parse_args()


class TerminalSession():

    def __init__(self, ip, port, domain, member, interface, outerface, description, agg, priority):
        self.ip = ip
        self.port = port
        self.domain = domain
        self.member = member
        self.interface = interface
        self.outerface = outerface
        self.description = description
        self.agg = agg
        self.priority = priority
        self.session = pexpect.spawn(f'telnet {self.ip} {self.port}')
        self.session.timeout = 5
        self.session.logfile = sys.stdout.buffer
        self.session.sendline('\r')
        out = self.session.expect(['Automatic configuration is running', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.send('\003')
        else:
            print('auto config isn\'t running')
        out = self.session.expect(['Press ENTER to get started', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('\r')

    def member_config(self):
        print(f'starting member config for member {self.member}')
        self.session.sendline('\r')
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.expect(']')
        self.session.sendline(f'irf domain {self.domain}')
        self.session.expect(']')
        self.session.sendline(f'irf member 1 renumber {self.member}')
        self.session.expect('Continue?')
        self.session.sendline('y')
        self.session.expect(']')
        self.session.sendline('save')
        self.session.expect('Are you sure?')
        self.session.sendline('y')
        self.session.expect('Please input the file name')
        self.session.sendline('\r')
        out = self.session.expect(['overwrite?', pexpect.TIMEOUT])
        if out == 0:
            self.session.sendline('y')
        self.session.expect('Saved the current configuration')
        self.session.sendline('quit')
        self.session.expect(']')
        self.session.sendline('reboot')
        # TODO: Figure out the exact wording for a reboot confirmation prompt
        self.session.expect('Continue?')
        self.session.sendline('y')
        return

    def irf_config(self):
        irf_commands = (
            f'interface range FortyGigE{self.member}/0/{self.interface} FortyGigE{self.member}/0/{self.outerface}',
            f'description {self.description}',
            'shutdown',
            'quit',
            f'irf-port {self.member}/1',
            f'port group interface FortyGigE{self.member}/0/{self.interface}',
            'quit',
            f'irf-port {self.member}/2',
            f'port group interface FortyGigE{self.member}/0/{self.outerface}',
            'quit',
            'irf mac-address persistent timer',
            'irf auto-update enable',
            'undo irf link-delay',
            f'irf member {self.member} priority {self.priority}',
            'irf mode normal',
        )

        self.session.sendline('\r')
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        for command in irf_commands:
            self.session.expect(']')
            self.session.sendline(command)
            time.sleep(0.5)

        self.session.expect(']')
        self.session.sendline('save')
        self.session.expect('Are you sure?')
        self.session.sendline('y')
        self.session.expect('Please input the file name')
        self.session.sendline('\r')
        self.session.expect('overwrite?')
        self.session.sendline('y')
        self.session.expect(']')
        self.session.sendline('quit')
        return

    def start_port_group(self):
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.expect(']')
        self.session.sendline(f'interface range FortyGigE{self.member}/0/{self.interface} FortyGigE{self.member}/0/{self.outerface}')
        self.session.expect(']')
        self.session.sendline('undo shutdown')
        return

    def save_config(self):
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.sendline('save')
        self.session.expect('Are you sure?')
        self.session.sendline('y')
        self.session.expect('Please input the file name')
        self.session.sendline('\r')
        out = self.session.expect(['overwrite?', pexpect.TIMEOUT])
        if out == 0:
            self.session.sendline('y')
        self.session.expect('Saved the current configuration')
        self.session.sendline('quit')
        self.session.expect(']')
        self.session.sendline('reboot')
        # TODO: Figure out the exact wording for a reboot confirmation prompt
        self.session.expect('Continue?')
        self.session.sendline('y')

    def agg_configure(self):
        agg_commands = (
            f'interface Bridge-Aggregation{self.agg}',
            'mad enable',
            f'{self.domain}',
        )

        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        for command in agg_commands:
            self.session.expect(']')
            self.session.sendline(command)

        self.session.expect(']')
        self.session.sendline('save')
        self.session.expect('Are you sure?')
        self.session.sendline('y')
        self.session.expect('Please input the file name')
        self.session.sendline('\r')
        self.session.expect('overwrite?')
        self.session.sendline('y')
        self.session.expect(']')
        self.session.sendline('quit')
        return

    def wait_for_boot(self, timeout=300):
        time.sleep(2)
        self.session.sendline('\r')
        out = self.session.expect(['Press ENTER to get started', '<HPE>', '[HPE]', pexpect.TIMEOUT], timeout=timeout)
        if out == 3:
            print(f'\n\nTimed out waiting for member {self.member} to come back up.\n \
            Login to 5900, confirm it\'s up. If timed out after member config reboot\n \
            then run script again with \"--skip-member-config {self.member}\" flag\n\n')
        elif out == 1 or out == 2:
            return
        else:
            self.session.sendline('\r')
            return

    def confirm_irf(self):
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.expect(']')
        self.session.sendline('display irf configuration')
        # TODO: Figure out expected output for a successful irf configuration

    def fix_fans(self):
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.expect(']')
        self.session.sendline('fan prefer-direction slot 1 port-to-power')
        return

    def irf_activate(self):
        out = self.session.expect(['<HPE>', pexpect.TIMEOUT], timeout=2)
        if out == 0:
            self.session.sendline('sys')
        self.session.expect(']')
        self.session.sendline('irf-port-configuration active')
        return


if __name__ == '__main__':
    print(args)
    tor1 = TerminalSession(
        ip=args.ip,
        port=args.port1,
        domain=args.domain,
        member=args.member1,
        interface=args.interface1,
        outerface=args.outerface1,
        description=args.description,
        agg=args.agg,
        priority=args.priority1,
    )

    tor2 = TerminalSession(
        ip=args.ip,
        port=args.port2,
        domain=args.domain,
        member=args.member2,
        interface=args.interface2,
        outerface=args.outerface2,
        description=args.description,
        agg=args.agg,
        priority=args.priority2,
    )

    if args.skip_member_config:
        if '1' in args.skip_member_config and '2' in args.skip_member_config:
            print('skipping member config for both members')
            pass
        elif '1' in args.skip_member_config:
            print('skipping member config for member 1')
            tor2.member_config()
        elif '2' in args.skip_member_config:
            print('skipping member config for member 2')
            tor1.member_config()
    else:
        tor1.member_config()
        tor2.member_config()

    tor1.wait_for_boot(600)
    tor1.irf_config()
    tor2.wait_for_boot(600)
    tor2.irf_config()
    tor1.irf_activate()

    tor2.wait_for_boot(600)
    if not args.skip_agg:
        tor1.agg_configure()
        tor2.agg_configure()
    if args.fans:
        tor1.fix_fans()
        tor2.fix_fans()
